import "react-native-gesture-handler";
import React from "react";
import { Provider } from "react-redux";

import Routes from "./src/Routes/index.routes";
import store from "./src/Redux/store";

export default function App() {
  return (
    <Provider store={store}>
      <Routes />
    </Provider>
  );
}
