import IProduct from './Product';
import IPedido from './Pedido';

interface IControl {
  nroComanda: string;
  created_at: Date;
  pedido: IPedido;
  items: IProduct[];
}


interface IControlState {
  active: IControl | null;
  list: IControl[];
}

export { IControl, IControlState };