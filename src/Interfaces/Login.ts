interface ILoginResponse {
    success: boolean;
    message: string;
    token: string;
    user: Object;
    account_bank: number;
    register: number;
    already: boolean;
}

interface ILoginRequest {
    login: string;
    password: string;
    terminal: Object;
}

export { ILoginResponse, ILoginRequest };