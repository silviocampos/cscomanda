interface IRetornoCreateVendaAberta {
    // pedido: Object
    _id?: number;
    terminal?: number;
    sequence?: string;
    number?: string | null;
    printed?: boolean;
    created_at?: string;
}

export default IRetornoCreateVendaAberta;