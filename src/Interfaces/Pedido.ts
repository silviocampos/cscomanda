import IProduct from '../Interfaces/Product';

interface IPedido {
    uuid: string;
    nroComanda: String;

    _id: number;   
    year: string,
    number: string,
    sequence: string,
    terminal: number,
    created_at: string,
    summary: {
      amount: number,
      discount: number,
      increase: number,
      change: number
    },
    type: string,
    cpf: string,
    note: string,
    status: string,
    user: {
      _id: number,
      name: string
    }

    items: IProduct[]  
  }
  
export default IPedido;