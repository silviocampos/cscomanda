interface ICategory {
    id: number;
    name: string;
    defaultImage: Function;
}

export default ICategory;