interface ISettings {
    terminal: number | null;
    baseURL: string | null;
    port: number,
    print:string | null
}
export default ISettings;