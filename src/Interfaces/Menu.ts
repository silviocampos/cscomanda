import { IProduct, ICategory } from '.';


interface IMenu {
    items: IProduct[];
    titles: ICategory[];
}

export default IMenu;