interface IProduct {
  _id: number;
  category: string;
  id_category: number;
  code: string;
  created_at: string;
  description: string;
  id_product: number;
  isActive: boolean;
  name: string;
  pccod: number;
  price: number;
  cst: string;
  ncm: string;
  origin: string;
  stock: number;
  unit: string;
  wholesalePrice: number;
  note?: string;
  qty: number;
  total: number;

  urlImage: String;
  b64Image: string;
  uuid: string;
}

export default IProduct;