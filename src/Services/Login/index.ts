import api from "../Api";
import { ILoginResponse, ILoginRequest, ISettings } from "../../Interfaces";
import BaseService from "../Base";

export default class Login extends BaseService {
    constructor() {

        super('auth', 'id');

        this.tableSchema = ` create table if not exists ${this.tablename} (id integer primary key not null, data text);`

        this.createDataBase()

    }


    async login({ login, password }: ILoginRequest, settings: ISettings ) {

        try {

            const response = await api.post<ILoginResponse, ILoginRequest>('/v2/login',
                { login, password, terminal: { register: settings.terminal } }
            )

            if (response.status === 200) {

                await this.save(response.data as ILoginResponse)
                
                return response.data as ILoginResponse
            } else {
                console.log( response.data )
                return undefined
            }

            
        } catch (error) {
            return false
        }

    }

    async save(data: ILoginResponse) {

        const ret = await this.database?.executeSql(`replace into auth (id, data) values (1, ?)`, [JSON.stringify(data)] as never[]);

    }

    async getCredentials() {

        try {
            const row = await this.findByKey(1 as never)

            if (row) return JSON.parse(row.data)
            else return {}

        } catch (error) {
            throw error
        }
    }

    async logout() {
        // nao da pra implementar o login aqui, pq um service nao pode ser chamado dentro de um hook ao que parece.
    }

}