import api from '../Api';
import { IControl, IRetornoCreateVendaAberta, IPedido, ISettings } from '../../Interfaces';
import BaseService from '../Base';

export default class ServiceControl extends BaseService {
    constructor() {
        super('control', 'id');
    }

    async selectControl(controlNumer: string) {
        if (!controlNumer) return;

        const response = await api.get<IPedido>(`/api/v2/venda-aberta/porComanda/${String(controlNumer)}`);
        const { data, status } = response;
        if (status != 200) {
            return null;
        }

        return data
    }

    async saveControl(control: IControl, settings: ISettings) {
        let result = <IRetornoCreateVendaAberta>{
        }

        try {
            const terminal = {
                nroTerminal: settings.terminal,
                codigoFilial: 1
            }


            const request = {
                print: settings.print,
                terminal,
                pedido: control.pedido
            }

            const response = await api.post('/api/v2/venda-aberta', request);
            const { data } = response as { data: IRetornoCreateVendaAberta };

            if (data && data.number) {

                const obj = { ...result, ...data };
                return obj

            } else {

                return null;
            }

        } catch (error) {
            //notificar por email?
            console.log(' erro ao gerar pedido:', error)
            return null;
        }

    }

    async selectOpened(terminal: number) {

        try {
            let result = undefined
            const response = await api.get<number, IControl[]>(`/api/v2/venda-aberta/porTerminal/${String(terminal)}`)

            if (response) {

                if (response.status === 404) return result;

                const { data } = response
                if (data) {

                    const serializedItems = await Promise.all((data).map(item => {
                        return {
                            nroComanda: item.controlNumber,
                            created_at: new Date(item.created_at),
                            pedido: {
                                _id: item._id,
                                number: item.number
                            }
                        }
                    }))

                    result = serializedItems
                }

            }


            return result
        } catch (error) {

            console.log("ServiceControl.selectOpened", error)
            return null

        }

    }

}