import * as SQLite from 'expo-sqlite';
import { shallowEqual } from 'react-redux';


interface IDatabaseServiceConstructor {
    dbname?: string;
    initTables?: boolean;
}

export default class DatabaseService {
    private _databaseName: string = '';
    public db!: SQLite.WebSQLDatabase;

    constructor({ dbname, initTables }: IDatabaseServiceConstructor) {

        // console.log( 'entrei no constructor', initTables,  Date.now() )

        let _dbName = 'cscomanda.db'
        if (dbname) _dbName = dbname

        this.connect(_dbName);

        if (initTables)
            this.createTables();

    }


    connect(dbName: string) {
        this._databaseName = dbName;
        this.db = SQLite.openDatabase(dbName);
        return this.db;
    }

    private async createTables() {
        try {
            const initialSetting = {
                terminal: 1,
                baseURL: 'localhost',
                port: 3000
            }

            await this.executeSql(` create table if not exists settings ( id integer primary key not null, data text ); `)
            // .then(res => console.log('settings table was created.', res))

            await this.executeSql('select * from settings', [])
                .then(async (result: SQLResultSet) => {
                    if (result.rows.length === 0) {
                        await this.executeSql(` replace into settings ( id, data ) values ( 1, ? ); `, [JSON.stringify(initialSetting) as never]);
                    }
                })

            await this.executeSql(` create table if not exists control ( id integer primary key not null, data text ); `)
            // .then(res => console.log('control table was created.', res))

            // this.db.transaction((tx: SQLTransaction) => {
            //     tx.executeSql(` create table if not exists control ( id integer primary key not null, data text ); `);

            //     // tx.executeSql(` insert into setting ( data ) values ( ? ); `, [JSON.stringify(initialSetting)]);
            //     // id integer primary key not null, 
            //     // terminal int, 
            //     // url_api text,
            //     // port_api int);
            // });
        } catch (err) {
            throw 'SQLite.createTables ' + JSON.stringify(err, null, 2);
            console.log('CARA deu ruim!', err)
        }
    }

    // async insert(table: string, data: any, fieldList: string[] = ['data']) {
    //     this.db.transaction((tx: SQLTransaction) => {
    //         tx.executeSql(`insert into ${table} (${fieldList}) values (0, ?)`, [data]);
    //         tx.executeSql(`select * from ${table}`, [], (_, { rows }) =>
    //             console.log(JSON.stringify(rows))
    //         );
    //     },
    //         (oi: SQLStatementCallback) => { console.log('oque veio nesse callback', oi) }
    //     );
    // }

    executeSql = async (sql: string, params = []) => {
        // console.log('1. QRY/PARAM sendo executada', sql, params)
        return new Promise<SQLResultSet>((resolve, reject) => {
            const sucesso = (transaction: SQLTransaction, resultSet: SQLResultSet) => {
                const { rows } = resultSet as { rows: SQLResultSetRowList };
                // console.log(`2.a dentro do result: EXCEUTE SQL [${sql}] `, resultSet, rows)
                // console.log(`2.b _`, transaction)
                resolve(resultSet);
            }

            const error = (transaction: SQLTransaction, error: SQLError) => {
                console.log(`SQLite.executeSql ERRO comando [${sql}]`, error);
                // reject(new Error (error));
                // return true;
                throw error
            };

            return this.db.transaction((tx: SQLTransaction) => {
                // console.log(`1.a antes do execute [${sql}]`)
                tx.executeSql(sql, params, sucesso, error)
            })
        })
    }

}