import DatabaseService from "../SQLite";
import BaseService from "../Base";
import api from "../Api";
import { ApiResponse, ApiOkResponse } from 'apisauce';

import { useDispatch } from "react-redux";
import { ISettings } from "../../Interfaces";

export default class SettingService extends BaseService {
    // public settings = useSelector<{ app: any }>((state) => state.app) as any
    public settings = {} as ISettings;

    private dispatch = useDispatch();

    constructor(initTables: boolean = false) {
        super('settings', 'id', initTables);
    }

    async getSettings() {

        try {
            const res = await this.findByKey(1 as never);
            if (res && res.length > 0) {

                const serialized = JSON.parse(res[0].data)

                this.settings = serialized;
                this.test();

                return serialized;

            } else {

                return {}

            }

        } catch (error) {
            console.error('ERRO => SettingService.getSettings: ', error)
        }

    }

    async test() {

        api.setBaseURL(`http://${this.settings.baseURL}:${this.settings.port}`);

        console.log(`http://${this.settings.baseURL}:${this.settings.port}`)

        const response = await api.post<null, ApiOkResponse<string>>('/ping?silient');

        let online = false;
        if (response && response.data) {

            const { message } = response.data as any;

            if (message)
                online = (message === 'pong');
        }

        // nao funciona =(
        this.dispatch({ type: 'SET_STATUS_API', online });

    }


    async saveSettings(data: any) {
        try {

            if (this.database) {

                await this.database.executeSql(`update settings set data = ? where id = 1`, [JSON.stringify(data) as never])
                return true;
                
            } else throw 'Database not assigned!'

        } catch (error) {
            console.error('ERRO => SettingService.saveSettings: ', error)
            throw error
            return false;
        }
    }


}
