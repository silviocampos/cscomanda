import * as SQLite from 'expo-sqlite';
import DatabaseService from "../SQLite";

export default class BaseService {
    public database?: DatabaseService;
    public tablename?: string;
    public key_fieldname?: string;
    public tableSchema: string = '';

    constructor(tablename: string, key_fieldname: string, initTables: boolean = false) {
        if (!tablename || !key_fieldname) throw 'Base service não iniciado corretamente!';

        try {
            this.database = new DatabaseService({ initTables });
            this.tablename = tablename;
            this.key_fieldname = key_fieldname;


            if (initTables && this.tableSchema) {
                this.createDataBase();
            }


        } catch (error) {
            console.log('BaseService => constructor', error)
        }

    }

    async createDataBase() {
        try {
            
            if (this.database)
                await this.database.executeSql(this.tableSchema)

        } catch (err) {
            throw 'BaseService.createTable ' + JSON.stringify(err, null, 2);
        }
    }


    async findAll() {

        try {
            if (this.database) {
                const { rows: { _array } } = await this.database.executeSql(`select * from ${this.tablename}`, []) as SQLResultSet;

                if (_array && _array.length > 0) {

                    return _array

                } else {

                    return undefined

                }
            } else {
                throw 'database not connected! '
            }

        } catch (error) {
            console.error('ERRO => BaseService.findAll: ', error)
        }

    }

    async findByKey(value: never) {

        try {
            if (this.database) {
                const { rows: { _array } } = await this.database.executeSql(`select * from ${this.tablename} where ${this.key_fieldname} = ? `, [value]) as SQLResultSet;

                if (_array && _array.length > 0) {

                    return _array

                } else {

                    return undefined

                }
            } else {
                throw 'database not connected! '
            }

        } catch (error) {
            console.error('ERRO => BaseService.findByKey: ', error)
        }

    }

    // async create(data: string) {
    //     try {

    //         await this.database.executeSql(`insert into ${table} values `, [data as never])
    //         return true;

    //     } catch (error) {
    //         console.error('ERRO => SettingService.saveSettings: ', error)
    //         throw error
    //         return false;
    //     }
    // }


}