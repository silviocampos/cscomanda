import { Alert } from 'react-native'
import { create } from "apisauce";
// import LoginService from './../Login';



const api = create({
  baseURL: "http://10.0.0.101:3000",
  headers: {
    'x-forwarded-for': '10.0.0.101'
  }
});

import store from '../../Redux/store'

// Prevent native splash screen from autohiding before App component declaration

api.addAsyncRequestTransform(async request => {
  const { auth } = store.getState()

  store.dispatch({ type: "SHOW_LOADING", loading: true })

  if (auth.token)
    request.headers = {
      'x-access-token': auth.token,
      'authorization': 'Basic Q0VOVFJF'
    }

})

api.addAsyncResponseTransform(async response => {

  store.dispatch({ type: "SHOW_LOADING", loading: false })

  if (!['200', '404'].includes(String(response.status))) {
    let message = response.data?.message || `Erro desconhecio no serviço local. ${response.config?.url}`;

    if (response.config?.url?.includes("ping")) {
      if (!response.config?.url?.includes("silient")) {
        message = 'Não foi possível conectar. Verifique o endereço do servidor; se o mesmo esteja ligado e/ou o serviço esteja funcionando.'
        Alert.alert(`👎🏻 Erro na requisição\n${response.config?.url}`, message)
        throw message
      }

    } else {

      if (response.config?.url?.includes("ping") && !response.config?.url?.includes("silient")) {

        Alert.alert(`Legal 👍🏻`, 'Consegui conectar no servidor.')

      }
    }
  }

  if (response.status === 401) {
    console.log(response)
    Alert.alert(`B.O!!`, 'Não esta autenticado.')
  }

  if (response.status === 500) {
    if (response.config?.url?.includes('testPrint')){
      Alert.alert(`Impressora não conectada.`, `End. Impressora = [${JSON.parse(response.config.data).print}]`)
      throw ''
    }
    else
      Alert.alert(`B.O!!`, `Erro no DB: [${response.data}]`)
  }
})



export default api;
