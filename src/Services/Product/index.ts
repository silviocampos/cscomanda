import BaseService from "../Base";
import api from "../Api";
import { IProduct, ICategory } from "../../Interfaces";

export default class ProductService extends BaseService {
  public products: IProduct[];
  public menu: ICategory[];

  constructor() {
    super('product', '_id');

    this.products = [] as IProduct[];
    this.menu = [] as ICategory[];

    this.tableSchema = `
            create table if not exists ${this.tablename} (
                id_product integer primary key not null, 
                description text,
                category text,
                id_category integer,
                code text,
                price real,
                cst text,
                ncm text,
                origin text,
                stock real,
                unit text,
                qty real,
                total real,
                urlImage text,
                b64Image text);
        `

        this.createDataBase()

  }

  async initilize() {
    try {
      
      await this.database?.executeSql(`delete from product`);

      const { data } = await api.get('/api/v2/product') as { data: IProduct[] };

      if (data && Array.isArray(data)) {
        for (const item of data) {
          const ret = await this.database?.executeSql(`
                         replace into product (id_product, description, category, id_category, code, price,
                             cst, ncm, origin, stock, unit, qty, total, urlImage, b64Image) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`, [
            item['id_product'], item['description'], item['category'], item['id_category'], item['code'], item['price'], item['cst'], item['ncm'], item['origin'], item['stock'], item['unit'], item['qty'], item['total'], item['urlImage'], item['b64Image']
          ] as never[]);

        }

      } else {
        console.log('deu ruim?? ', data)
      }

    } catch (error) {
      console.log(' oque deu aqui', error)
    }

    await this.getMenu()
  }


  async getMenu() {
    this.products = await this.findAll();

    if (this.products) {
      const listOfCategories = await Promise.all(this.products.map(item => item.category));

      this.menu = await Promise.all(Array.from(new Set(listOfCategories))
        .map(item => {
          const found = this.products.find(product => product.category === item)
          let id = 0
          if (found) {
            id = found.id_category;
          }

          return {
            id,
            name: item,
            defaultImage: () => {
              switch (item) {
                case 'PRATO FEITO':
                  return 'food';
                case 'BEBIDAS':
                  return 'drink';
                default:
                  return ''
              }
            }
          };
        })
        .sort((a, b) => a.id < b.id ? -1 : 1));
    }
  }



}