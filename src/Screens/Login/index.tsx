import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigation } from "@react-navigation/native";

import {
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
  Text,
  TextInput,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { RectButton } from 'react-native-gesture-handler'

import globalStyle from "../../Style";

import { ILoginRequest, ISettings, ILoginResponse } from "../../Interfaces";
import ServiceControl from "../../Services/Control";
import Colors from "../../Style/colors";
import SettingService from "../../Services/Setting";
import ProductService from "../../Services/Product";
import LoginService from "../../Services/Login";
import Dialog from "../../Util/Dialog";


export default function Login() {
  const settings = useSelector<{ settings: ISettings }>((state) => state.settings) as ISettings;
  const online = useSelector<{ online: boolean }>((state) => state.online) as boolean;
  const auth = useSelector<{ auth: ILoginResponse }>((state) => state.auth) as ILoginResponse;


  const [authData, setAuthData] = useState<ILoginRequest>({ login: '', password: '' } as ILoginRequest);
  const [localSettings, setLocalSettings] = useState<ISettings>({} as ISettings);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const loginService = new LoginService();
  const controlService = new ServiceControl();
  const settingsService = new SettingService(true);
  const productService = new ProductService();


  async function sincronizeMenu() {

    await productService.initilize()

    const products = productService.products;
    const menu = productService.menu;

    const { terminal } = settings as ISettings;

    if (terminal) {

      const openedControls = await controlService.selectOpened(terminal);

      if (openedControls)
        dispatch({ type: 'ADD_LIST_CONTROL', controls: openedControls });

    }

    dispatch({ type: 'UPDATE_MENU', menu: products })
    dispatch({ type: 'UPDATE_TITLE_MENU', titles: menu })
  }

  async function handlerAuth() {

    if (authData.login && authData.password) {
      const credentials = await loginService.login(authData, localSettings);
      if (credentials) {

        dispatch({ type: 'SET_AUTH', data: credentials })


        await sincronizeMenu();


      } else {
        Dialog.showAlert({ message: 'Problema! Certamente não esta autenticado.' })
      }
    } else {
      Dialog.showAlert({ message: 'Verifique as credenciais antes de clicar em Entrar.' })
    }
  }

  useEffect(() => {

    const checkSettings = async () => {

      const data = await settingsService.getSettings();

      dispatch({ type: 'SET_APP', data });
      setLocalSettings(data);


    }

    checkSettings();


  }, [])

  return (
    <KeyboardAvoidingView
      behavior="padding"
      enabled={Platform.OS === "ios"}
      style={styles.container}
    >
      <Text style={styles.appName}>CSComanda</Text>
      <Text style={styles.subTitle}>um produto Centre Soluções</Text>
      <TextInput
        style={globalStyle.input}
        placeholder="Login"
        value={authData.login}
        onChangeText={(text) => setAuthData({ ...authData, login: text })}
      ></TextInput>
      <TextInput
        style={globalStyle.input}
        placeholder="Senha"
        value={authData.password}
        textContentType="password"
        secureTextEntry={true}
        onChangeText={(text) => setAuthData({ ...authData, password: text })}
      ></TextInput>
      <RectButton
        style={globalStyle.button}
        onPress={() => handlerAuth()}
      >
        <Text style={globalStyle.labelButton}>Entrar</Text>
      </RectButton>

      <RectButton
        style={styles.buttonSettings}
        onPress={() => navigation.navigate("Settings")}
      >
        <Text style={styles.labelButtonSettings}>Configurações</Text>
        <Text>
          <Ionicons
            name={online ? "md-checkmark" : "md-close"}
            style={[styles.icon, { color: online ? 'green' : 'red' }]} /> {online ? 'Connectado' : 'Desconectado'}
        </Text>

      </RectButton>



    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF",
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 16,
  },
  appName: {
    fontSize: 26,

    fontWeight: "bold",
  },
  subTitle: {
    fontSize: 9,
    marginBottom: 36,
  },
  containerForm: {
    backgroundColor: "#DDD",
  },
  buttonSettings: {
    ...globalStyle.button,
    backgroundColor: '#FFF',
    marginTop: 10
  },
  labelButtonSettings: {
    ...globalStyle.labelButton,
    color: Colors.Primary,
  },
  cancelled: {
    color: Colors.Danger,
  },
  icon: {
    marginTop: 16,
    marginRight: 16,
    fontSize: 32,
    color: 'green',
  },
});

