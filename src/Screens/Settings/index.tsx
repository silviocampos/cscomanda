import React, { useState, useEffect } from "react";

import { StyleSheet, SafeAreaView, View, Text, TextInput, Alert, ScrollView } from 'react-native'
import { RectButton } from 'react-native-gesture-handler'
import Colors from "../../Style/colors";
import { useSelector, useDispatch } from "react-redux";
import api from "../../Services/Api";
import { useNavigation } from "@react-navigation/native";
import SettingService from "../../Services/Setting";
import { ISettings } from "../../Interfaces";
import store from "../../Redux/store";

export default function Setttings() {

    const navigation = useNavigation()
    const appSettings = useSelector<{ settings: ISettings }>((state) => state.settings);
    const service = new SettingService(false);

    const [appSettingsData, setAppSettingsData] = useState<ISettings>(appSettings as ISettings);

    // const dispatch = useDispatch()

    async function handlerSetAppSettings() {

        await service.saveSettings(appSettingsData)

        store.dispatch({ type: 'SET_APP', data: appSettingsData });
        // dispatch({ type: 'SET_APP', data: appSettingsData });

        return testConnect()
    }

    async function testConnect() {

        testPrint(false)

        api.setBaseURL(`http://${appSettingsData.baseURL}:${appSettingsData.port}`)

        try {

            await api.post('/ping');

            navigation.navigate("Login");

        } catch (error) {

            navigation.navigate("Settings");
        }

    }

    async function testPrint(alert: boolean = true) {
        await api.post('/testPrint', { print: String(appSettingsData.print).toLowerCase() })
        if (alert) Alert.alert('Ok', 'Impressora conecta com sucesso.')
    }

    useEffect(() => {
        const getSettings = async () => {

            const settings = await service.getSettings();
            if (settings) {

                setAppSettingsData(settings);

            } else {
                console.log(`Settings => useEffect => getSettings: Não encontrou registro na tabela settings. 
                             Possivelmente o database não foi inicializado corretamente.`);
            }

        }

        getSettings();
    }, [])

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.title}>
                    Essas configurações são necessárias para comunicação e funcionamento do sistema.
                </Text>
            </View>
            <ScrollView
                horizontal={false}
                showsHorizontalScrollIndicator={false}
                style={styles.controlTab}
            >
                <View style={styles.list}>


                    <Text style={styles.headerText}>Nº do terminal</Text>
                    <TextInput
                        keyboardType="numeric"
                        placeholder="Ex: 10"
                        style={styles.itemInput}
                        value={String(appSettingsData.terminal)}
                        onChangeText={text => setAppSettingsData({ ...appSettingsData, terminal: Number(text) })}
                    />

                    <Text style={styles.headerText}>URL Servidor</Text>

                    <TextInput
                        placeholder="Ex: 192.168.0.100"
                        style={styles.itemInput}
                        value={String(appSettingsData.baseURL)}
                        onChangeText={text => setAppSettingsData({ ...appSettingsData, baseURL: text })}
                    />

                    <Text style={styles.headerText}>Nº Porta</Text>

                    <TextInput
                        keyboardType="numeric"
                        style={styles.itemInput}
                        value={String(appSettingsData.port)}
                        onChangeText={text => setAppSettingsData({ ...appSettingsData, port: Number(text) })}
                    />

                    <Text style={styles.headerText}>End. Impressora</Text>

                    <TextInput
                        placeholder="Ex: 192.168.0.101"
                        style={styles.itemInput}
                        value={appSettingsData.print as string}
                        onChangeText={text => setAppSettingsData({ ...appSettingsData, print: text })}
                    />
                    <RectButton style={styles.button} onPress={() => testPrint()}>
                        <Text style={styles.textButton}>Testar impressora</Text>
                    </RectButton>

                </View>
            </ScrollView>

            <View>
                <RectButton style={styles.button} onPress={() => handlerSetAppSettings()}>
                    <Text style={styles.textButton}>Salvar</Text>
                </RectButton>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF",
    },
    controlTab: {
        height: 56,
        backgroundColor: "#FFF",
        elevation: 3,
    },
    header: {
        padding: 16
    },
    title: {
        fontSize: 18
    },
    list: {
        flex: 1,
        padding: 16
    },
    headerList: {
        padding: 16,
        backgroundColor: '#fcfcfc',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: '#fbfbfb'
    },
    headerText: {
        color: '#777'
    },
    item: {

    },
    itemInput: {
        paddingHorizontal: 16,
        paddingVertical: 18,

        backgroundColor: "#f5f5f5",
        padding: 16,
        // fontSize: 18,
        textAlign: "center",

    },
    button: {
        backgroundColor: Colors.Primary,
        padding: 16,
    },
    textButton: {
        color: "#FFF",
        fontWeight: 'bold',
        textAlign: 'center'
    }
});
