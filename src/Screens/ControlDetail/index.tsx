import React, { useState, useEffect } from "react";
import { RectButton, TouchableOpacity } from 'react-native-gesture-handler'

import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    TextInput,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { useSelector } from "react-redux";
import { useNavigation } from '@react-navigation/native';
const uuid = require('random-uuid-v4');

import Colors from "../../Style/colors";
import globalStyle from "../../Style";
import { IControlState } from '../../Interfaces';

export default function ControlDetail() {
    const navigation = useNavigation()

    const { active: activeControl } = useSelector((state: any) => state.control) as IControlState;

    return (
        <>
            <ScrollView style={styles.container}>
                <View style={styles.box}>
                    <Text style={styles.title}>Itens da comanda</Text>
                    <View style={styles.listItem}>
                        {activeControl?.items?.map((item, index) => (
                            <RectButton
                                key={index}
                                style={[styles.buttonItem, index === 0 && styles.buttonItemFirst]}
                            >
                                <Ionicons name="md-basket" style={styles.icon} />
                                <View>
                                    <Text style={styles.comandaLabel}>{item.qty} x {item?.description}</Text>
                                    <Text style={styles.comandaCount}>{item?.note}</Text>
                                </View>
                            </RectButton>
                        ))}
                    </View>
                </View>
            </ScrollView>
            <View style={styles.footer}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={globalStyle.button}
                    onPress={() => { navigation.navigate('Home') }}
                >
                    <Text style={globalStyle.labelButton}>Adicionar Itens</Text>
                </TouchableOpacity>
            </View>
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF",
    },
    box: {},
    title: {
        fontSize: 20,
        fontWeight: "bold",
        margin: 16,
    },
    listItem: {},
    buttonItem: {
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        padding: 16,
        backgroundColor: "#FFF",
        borderBottomWidth: 1,
        borderBottomColor: "#EEE",
    },
    buttonItemFirst: {
        borderTopWidth: 1,
        borderTopColor: "#EEE",
    },
    icon: {
        marginRight: 16,
        fontSize: 32,
        color: Colors.Primary,
    },
    comandaLabel: {
        fontSize: 18,
        fontWeight: "bold",
        color: "#666",
    },
    comandaCount: {
        color: "#999",
    },
    inputSearch: {
        backgroundColor: "#f5f5f5",
        padding: 16,
        fontSize: 18,
        textAlign: "center",
    },
    buttonAdd: {
        padding: 16,
        backgroundColor: Colors.Primary
    },
    labelButtonAdd: {
        textAlign: "center",
        color: "#FFF",
        fontWeight: "bold"
    },
    footer: {
        backgroundColor: "#FFF",
        height: 60,
        paddingHorizontal: 16,
        paddingTop: 7,
    },
});
