import React, { useState, useEffect } from "react";
import { useNavigation } from "@react-navigation/native";
import { useSelector } from "react-redux";

import {
  StyleSheet,
  SafeAreaView,
  Text,
  ScrollView,
  TouchableOpacity,
  View,
  Image,
  Animated,
} from "react-native";
import { useDispatch } from "react-redux";
const uuid = require('random-uuid-v4');

import styles from "./style";
import SheetQty from "../../Components/Home/SheetQty";
import globalStyle from "../../Style";
import { IProduct, ICategory, IControlState, IMenu } from '../../Interfaces'
import images from "../../Images";
import currency from "../../Util/currency";



export default function Home() {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [item, setItem] = useState({}) as any;
  const [categoryActive, setCategoryActive] = useState<ICategory>({} as ICategory);
  const [itemsCategory, setItemsCategory] = useState<IProduct[]>([]);

  const cart = useSelector((state: any) => state.cart);
  const control = useSelector((state: any) => state.control) as IControlState;

  const {
    items: itemsMenu,
    titles: categories
  } = useSelector((state: any) => { return state.menu }) as IMenu;

  useEffect(() => {

    if (categories && categories.length > 0)
      handlerChangeCategory(categories[0]);

  }, [categories])

  useEffect(() => {

    dispatch({ type: 'CLEAR_CART', action: { alert: false } });

  }, [])



  function handlerChangeCategory(category: ICategory) {

    setCategoryActive(category);
    const filteredItems = itemsMenu.filter(el => el.id_category === category.id)
    setItemsCategory(filteredItems)

  }

  function handlerCheckout() {

    if (control.active?.nroComanda)
      navigation.navigate("Checkout");
    else
      navigation.navigate("Control", { goTo: 'Checkout' })

  }

  function handlerAddItemCart(itemList: any) {

    itemList.uuid = uuid();
    setItem({ ...itemList, qty: 1 });

  }

  return (
    <SafeAreaView style={styles.container}>
      <View>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          style={styles.controlTab}
        >
          {categories?.map((category) => (
            <TouchableOpacity
              activeOpacity={1}
              key={category.id}
              style={[
                styles.tabButton,
                categoryActive.id === category.id && styles.tabButtonActive,
              ]}
              onPress={() => handlerChangeCategory(category)}
            >
              <Text
                style={[
                  styles.tabLabel,
                  categoryActive.id === category.id && styles.tabLabelActive,
                ]}
              >
                {category.name}
              </Text>
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
      {/*<View style={styles.inputSearchBox}>
        <TextInput
          style={globalStyle.input}
          placeholder={`Filtrar em ${categories[
            categoryActive
          ].toLowerCase()} ...`}
        ></TextInput>
      </View> */}
      <ScrollView>
        <View style={styles.itemsList}>
          {itemsCategory?.map((item) => (
            <TouchableOpacity
              activeOpacity={1}
              key={item.code}
              style={styles.boxItem}
              onPress={() => handlerAddItemCart(item)}
            >
              <Image
                style={styles.itemImage}
                source={{ uri: images.parse(categoryActive.defaultImage()) }}
              />
              <View style={styles.itemInfo}>
                <Text style={styles.itemName}>{item.description}</Text>
                <Text style={styles.itemName}>{currency(item.price)}</Text>
              </View>
            </TouchableOpacity>
          ))}
        </View>
      </ScrollView>
      
      <View style={styles.footer}>
        {item?.id_product && <SheetQty item={item} setItem={setItem} change={false} />}
        {!item.id_product && cart.length > 0 && (
          <TouchableOpacity
            activeOpacity={0.8}
            style={[
              globalStyle.button,
              { marginHorizontal: 16, marginBottom: 8 },
            ]}
            onPress={handlerCheckout}
          >
            <Text style={globalStyle.labelButton}>
              Revisar {cart.length} items
            </Text>
          </TouchableOpacity>
        )}
      </View>
    </SafeAreaView>
  );
}


// Icons made by <a href="https://www.flaticon.com/authors/monkik" title="monkik">monkik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>