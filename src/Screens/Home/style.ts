import { StyleSheet } from "react-native";
import Colors from "../../Style/colors";
import { Directions } from "react-native-gesture-handler";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF",
  },
  controlTab: {
    height: 56,
    backgroundColor: "#FFF",
    elevation: 3,
  },
  tabButton: {
    height: 56,
    padding: 16,
  },
  tabButtonActive: {
    borderBottomColor: Colors.Primary,
    borderBottomWidth: 3,
  },
  tabLabel: {
    fontSize: 18,
  },
  tabLabelActive: {
    fontWeight: "bold",
    color: Colors.Primary,
  },
  inputSearchBox: {
    paddingHorizontal: 16,
    paddingBottom: 16,
    backgroundColor: "#FFF",
  },
  itemsList: {
    paddingHorizontal: 8,
    paddingTop: 8,
    flex: 1,
    // display: "flex",
    flexWrap: "wrap",        
    alignItems: "flex-start",
    // flexDirection: 'column',
    flexDirection: "row", 
    justifyContent: "space-between",
    alignContent: "space-between",

  },
  boxItem: {
    height: 110,
    width: "48%",
    borderRadius: 4,
    marginBottom: 16,
    alignItems: "center",
    position: "relative",
    elevation: 2,
    backgroundColor: "#FFF",
  },
  itemImage: {
    width: 85,
    height: 85,
    maxHeight: 140,
    borderRadius: 4,
  },
  itemInfo: {
    position: "absolute",
    bottom: 0,
    textAlign: "left",
    width: "100%",
    padding: 8,
    backgroundColor: "#333",
    borderBottomLeftRadius: 4,
    borderBottomEndRadius: 4,
  },
  itemName: {
    color: "#FFF",
  },
  itemPrice: {
    color: "#FFF",
  },
  footer: {
    backgroundColor: "#FFF",
    paddingTop: 7,
  },
});

export default styles;
