import React, { useState } from "react";

import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";

import { RectButton } from 'react-native-gesture-handler'

const uuid = require('random-uuid-v4');
import { Ionicons } from "@expo/vector-icons";
import { useSelector, useDispatch } from "react-redux";
import { useNavigation } from "@react-navigation/native";

import Colors from "../../Style/colors";
import globalStyle from "../../Style";

import api from "../../Services/Api";
import currency from '../../Util/currency';
import Dialog from '../../Util/Dialog';
import store from '../../Redux/store'

import { IProduct, IControlState, IControl, ISettings } from "../../Interfaces";
import SheetQty from "../../Components/Home/SheetQty";
import ServiceControl from "../../Services/Control";


export default function Checkout() {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const service = new ServiceControl();

  const settings = useSelector((state: any) => state.settings) as ISettings;
  const cart = useSelector((state: any) => state.cart) as IProduct[];
  const control = useSelector((state: any) => state.control) as IControlState;

  const [onRequest, setOnRequest] = useState(false);
  const [itemChanging, setItemChanging] = useState<IProduct>({} as IProduct);


  async function handlerSendCart() {
    try {
      if (onRequest) return;

      setOnRequest(true);
      store.dispatch({ type: "SHOW_LOADING", loading: true })

      if (!control.active) {
        console.log('Cara, comanda nao esta ativa?!??!??!')!
        return;
      }

      control.active.pedido.items = cart;
      control.active.pedido.nroComanda = control.active.nroComanda;
      console.log(settings)
      const data = await service.saveControl(control.active, settings);
      console.log({ data })
      if (data) {

        Dialog.showToast(`Pedido incluído! #${data.number}`)

        dispatch({ type: "CLEAR_CART" });
        dispatch({ type: 'CLEAR_ACTIVE_CONTROL' });

        store.dispatch({ type: "SHOW_LOADING", loading: false });

        setOnRequest(false);
        navigation.navigate("Control");


      } else {

        return Dialog.showToast(`Problema ao incluir pedido!`)

      }
    } catch (error) {
      throw 'Checkout.handlerSendCart ' + error
    }


  }

  function handleChangeItem(item: IProduct) {
    setItemChanging(item)
  }

  return (
    <>
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <Text style={styles.title}>Comanda Nº {control?.active?.nroComanda}</Text>
          <View style={styles.listItem}>
            {cart.map((_item: any, index: number) => (
              <TouchableOpacity
                key={index}
                style={[styles.buttonItem, index === 0 && styles.buttonItemFirst]}
                onPress={() => handleChangeItem(_item)}
              >
                <View>
                  <Ionicons name="md-pricetag" style={styles.icon} />
                </View>
                <View style={styles.comandaContainerInfoItem}>
                  <View>
                    <Text style={styles.comandaLabel}> {_item.qty}x {_item.description}</Text>
                    <Text style={styles.comandaNote}>{_item.note}</Text>
                  </View>
                  <View>
                    <Text style={styles.comandaCount}>{currency(_item.price)}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            ))}
          </View>
        </ScrollView>
        {!itemChanging?.id_product && (

          <>
            <View style={styles.subFooter} >
              <Text>Total Items: {cart.length}</Text>
            </View>
            <View style={styles.footer}>
              <RectButton
                activeOpacity={0.8}
                style={globalStyle.button}
                onPress={handlerSendCart}
              >
                <Text style={globalStyle.labelButton}>Enviar pedido</Text>
              </RectButton>
            </View>
          </>
        )}


      </SafeAreaView>
      <>
        {itemChanging?.id_product && <SheetQty change={true} item={itemChanging} setItem={setItemChanging} />}
      </>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF",
  },
  box: {},
  title: {
    fontSize: 20,
    fontWeight: "bold",
    margin: 16,
  },
  listItem: {
  },
  buttonItem: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    padding: 16,
    backgroundColor: "#FFF",
    borderBottomWidth: 1,
    borderBottomColor: "#EEE",
  },
  buttonItemFirst: {
    borderTopWidth: 1,
    borderTopColor: "#EEE",
  },
  icon: {
    marginRight: 16,
    fontSize: 32,
    color: Colors.Primary,
  },
  comandaContainerInfoItem: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  comandaLabel: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#666",
  },
  comandaNote: {

  },
  comandaCount: {
    textAlign: "right",
    color: "#999",
  },
  inputSearch: {
    backgroundColor: "#f5f5f5",
    padding: 16,
    fontSize: 18,
    textAlign: "center",
  },
  subFooter: {
    paddingHorizontal: 16
  },
  footer: {
    backgroundColor: "#FFF",
    height: 60,
    paddingHorizontal: 16,
    paddingTop: 7,
  },
});
