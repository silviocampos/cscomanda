import React, { useState, useEffect } from "react";
import { RectButton } from 'react-native-gesture-handler'
const pkg = require('../../../package.json');

import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TextInput,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { useNavigation, useNavigationState, useRoute } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
const io = require('socket.io-client');

const uuid = require('random-uuid-v4');

import Colors from "../../Style/colors";
import globalStyle from "../../Style";
import { IControl, IControlState, IPedido, IProduct, ISettings } from '../../Interfaces';
import ServiceControl from "../../Services/Control";

export default function Control() {
  const [controlNumber, setControlNumber] = useState('')
  const controls = useSelector((state: any) => state.control) as IControlState;
  const settings = useSelector((state: any) => state.settings) as ISettings;

  const [appVersion, setAppVersion] = useState('');


  const [dataSocket, setDataSocket] = useState({});

  const navigation = useNavigation()
  const { params } = useRoute() as any

  const dispatch = useDispatch()
  const service = new ServiceControl();

  //socket aqui add ou remover do state de controls
  // useEffect(() => {
  //   this.socket = io(`http://${settings.baseURL}:${settings.port}`, {
  //     transports: ['websocket'],
  //   });

  //   this.socket.on('connect', () => {
  //     setDataSocket({ ...dataSocket, ...{ isConnected: true } });

  //   });

  //   this.socket.on('ping', data => {
  //     setDataSocket({ ...dataSocket, ...{ data } });

  //   });
  // }, [])

  useEffect( ()=> {
    const fetchData = async () => {
      setAppVersion('v. ' + pkg.version || '1.0.0');

      const openedControls = await service.selectOpened(-1);

      if (openedControls)
        dispatch({ type: 'ADD_LIST_CONTROL', controls: openedControls });
    }

    fetchData();   
  }, [])


  async function handlerControl(control: IControl) {
    if (!control.pedido) control.pedido = {} as IPedido;

    const orderFound = await service.selectControl(control.nroComanda);
    if (orderFound) {
      control.pedido._id = orderFound._id;
      control.items = orderFound.items;
    } else {
      control.pedido._id = 0;
      control.items = []
    }

    control.pedido.uuid = uuid();

    dispatch({ type: 'ADD_CONTROL', control })

    if (!control.items || control.items.length === 0) {

      navigation.navigate(params?.goTo || 'Home')

    } else {

      navigation.navigate('ControlDetail')

    }
    setControlNumber('')
  }

  return (
    <ScrollView style={styles.container}>
      <View style={styles.box}>
        <View style={styles.label}>
          <Text style={styles.title}>Adicionar</Text>
          <Text style={styles.version}>{appVersion}</Text>
        </View>
        <TextInput
          keyboardType="number-pad"
          style={styles.inputSearch}
          placeholderTextColor={"#999"}
          placeholder="Nº da comanda"
          value={controlNumber}
          onChangeText={(text) => { setControlNumber(text.replace(/[^0-9]/g, '')) }}
        />
        <RectButton
          activeOpacity={.8}
          style={styles.buttonAdd}
          onPress={() => handlerControl({ nroComanda: controlNumber || 0, created_at: new Date() } as IControl)}
        >
          <Text style={styles.labelButtonAdd}>Adicionar</Text>
        </RectButton>
      </View>
      <View style={styles.box}>
        <Text style={styles.title}>Comandas em aberto</Text>
        <View style={styles.listItem}>
          {controls?.list?.sort((a, b) => ((a.created_at > b.created_at) * -1)).map((item, index) => (
            <RectButton
              key={index}
              style={[styles.buttonItem, index === 0 && styles.buttonItemFirst]}
              onPress={() => handlerControl(item)}
            >
              <View>
                <Ionicons name="md-list-box" style={styles.icon} />
              </View>
              <View>
                <Text style={styles.comandaLabel}>Nº {item?.nroComanda}</Text>
                <Text style={styles.comandaCount}>COMANDA</Text>
              </View>
            </RectButton>
          ))}
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF",
  },
  box: {},
  label: {
    flex: 1, 
    flexDirection: "row",
    alignItems: "flex-end"
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    margin: 16,
  },
  version: {
    fontSize: 8,
    margin: 16,
    marginLeft: "auto"    
  },
  listItem: {},
  buttonItem: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    padding: 16,
    backgroundColor: "#FFF",
    borderBottomWidth: 1,
    borderBottomColor: "#EEE",
  },
  buttonItemFirst: {
    borderTopWidth: 1,
    borderTopColor: "#EEE",
  },
  icon: {
    marginRight: 16,
    fontSize: 32,
    color: Colors.Primary,
  },
  comandaLabel: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#666",
  },
  comandaCount: {
    color: "#999",
  },
  inputSearch: {
    backgroundColor: "#f5f5f5",
    padding: 16,
    fontSize: 18,
    textAlign: "center",
  },
  buttonAdd: {
    padding: 16,
    backgroundColor: Colors.Primary
  },
  labelButtonAdd: {
    textAlign: "center",
    color: "#FFF",
    fontWeight: "bold"
  }
});
