import React from 'react';
import { ToastAndroid, Alert, AlertButton } from 'react-native';


const showToast = (message: string) => {
  // ToastAndroid.show('A pikachu appeared nearby !', ToastAndroid.SHORT);
  ToastAndroid.showWithGravity(message, ToastAndroid.SHORT, ToastAndroid.CENTER);
  // ToastAndroid.showWithGravityAndOffset(
  //   'A wild toast appeared!',
  //   ToastAndroid.LONG,
  //   ToastAndroid.BOTTOM,
  //   25,
  //   50
  // );
};

interface AlertProps {
  title?: string;
  message?: string;
  blockScreen?: boolean;
  isQuestion?: boolean;
  actionYes?: (value?: string) => void;
  actionNo?: (value?: string) => void;
}

const showAlert = ({ title, message, blockScreen, isQuestion, actionYes, actionNo }: AlertProps) => {
  const buttons = [] as AlertButton[]
  if (isQuestion) {
    buttons.push({ text: 'Sim', onPress: actionYes })
    buttons.push({ text: 'Não', onPress: actionNo })
  }

  Alert.alert(
    title || 'Alerta',
    message || 'Mensagem Padrão',
    buttons,
    { cancelable: !blockScreen }
  )

}

const Dialog = {
  showToast,
  showAlert
}

export default Dialog;