import currencyFormatter from 'currency-formatter'
import * as Localization from 'expo-localization'




const currency = (value:number, currencyCode:string = 'BRL', locale = Localization.locale) => {

    try {
        const optionsFormat = { style: 'currency', currency: currencyCode || 'BRL' }
        return new Intl.NumberFormat(locale || 'pt-BR', optionsFormat).format(value);
    } catch (error) {
        return currencyFormatter.format(value,{locale:'pt-BR',code:currencyCode})
    }

}

export default currency;