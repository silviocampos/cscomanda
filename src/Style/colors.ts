const Colors = {
  Primary: "#2dd1ac",
  Danger: "#dc3545",
  Success: "#28a745",
};

export default Colors;
