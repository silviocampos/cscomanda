import { StyleSheet } from "react-native";
import Colors from "./colors";

const globalStyle = StyleSheet.create({
  input: {
    height: 56,
    paddingHorizontal: 15,
    marginVertical: 10,
    borderWidth: 1,
    borderRadius: 4,
    borderColor: "#CCC",
    backgroundColor: "#FFF",
    alignSelf: "stretch",
  },
  button: {
    height: 46,
    alignSelf: "stretch",
    backgroundColor: Colors.Primary,
    borderRadius: 4,
    justifyContent: "center",
    alignItems: "center",
  },
  labelButton: {
    color: "#FFF",
    fontWeight: "bold",
    fontSize: 18,
  },
});
export default globalStyle;
