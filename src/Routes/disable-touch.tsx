import React from 'react'
import { SafeAreaView, ActivityIndicator } from 'react-native';

interface Props {
    disableTouch?: boolean;
}

const DisableTouch: React.FC<Props> = ({ disableTouch, children }) => {
    return (
        <>


            <SafeAreaView style={{ flex: 1 }} pointerEvents={disableTouch ? 'none' : 'auto'}>
                {children}
            </SafeAreaView>
            {disableTouch && <ActivityIndicator size="large" />}

        </>
    )
}

export default DisableTouch;