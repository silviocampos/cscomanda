
import 'react-native-gesture-handler';
import React from "react";
import { useSelector } from "react-redux";
import { TapGestureHandler } from "react-native-gesture-handler";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";

import Login from "../Screens/Login";
import Home from "../Screens/Home";
import HeaderButtons from "../Components/Home/Header";
import LogoutHeaderButton from "../Components/Control/Header";
import Control from "../Screens/Control";
import ControlDetail from "../Screens/ControlDetail";
import Settings from "../Screens/Settings";

import DisableTouch from './disable-touch';
import Checkout from '../Screens/Checkout';

const Stack = createStackNavigator()
const Drawer = createDrawerNavigator()

const optionsScreens = {
  Home: {
    title: "Vender",
    headerStyle: {
      elevation: 0,
    },
    headerTitleStyle: {
      fontWeight: "bold",
    },
    headerRight: () => <HeaderButtons />,
  },
  Control: {
    title: "Selecionar comanda",
    headerRight: () => <LogoutHeaderButton />

  },
  ControlDetail: {
    title: "Detalhes da Comanda:",
  },
  Checkout: {
    title: "Revisar pedido",
  },
  Settings: {
    title: 'Configurações'
  }
}



export default function Routes() {

  const { loading, auth } = useSelector((state: any) => state)

  function renderScreens() {
    return (
      <>

        {
          !auth.token ?
            <Stack.Screen name="Login" component={Login} />
            :
            (
              <>
                <Stack.Screen
                  name="Control"
                  component={Control}
                  options={optionsScreens.Control}
                />
                <Stack.Screen
                  name="ControlDetail"
                  component={ControlDetail}
                  options={optionsScreens.ControlDetail}
                />

                <Stack.Screen
                  component={Home}
                  name="Home"
                  options={optionsScreens.Home}
                />

                <Stack.Screen
                  component={Checkout}
                  name="Checkout"
                  options={optionsScreens.Checkout}
                />
              </>
            )
        }




      </>
    )
  }

  return (
    <DisableTouch disableTouch={loading} >
      <NavigationContainer>
        <Stack.Navigator>
          {renderScreens()}
          <Stack.Screen
            component={Settings}
            name="Settings"
            options={optionsScreens.Settings}
          />
        </Stack.Navigator>
      </NavigationContainer >
    </DisableTouch >
  );
}