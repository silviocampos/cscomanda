import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import Login from "../Screens/Login";
import Home from "../Screens/Home";
import Control from "../Screens/Control";

import HeaderButtons from "../Components/Home/Header";
import Checkout from "../Screens/Checkout";
import { StyleSheet, ActivityIndicator, View, Dimensions } from "react-native";
import Colors from "../Style/colors";
import { useSelector } from "react-redux";

const Stack = createStackNavigator();

export default function AuthRouter() {

    const loading = useSelector((state: any) => state.loading)

    return (
        <>
            {<View style={[styles.container, styles.horizontal]}>
                    <ActivityIndicator size="large" color={Colors.Primary} />
                </View>}

            {
                loading && <View style={[styles.container, styles.horizontal]}>
                    <ActivityIndicator size="large" color={Colors.Primary} />
                </View>
            }
            {
                !loading && (

                    <Stack.Navigator initialRouteName="Control" screenOptions={{ cardStyle: { backgroundColor: '#FFF' } }}>
                    <Stack.Screen
                        component={Home}
                        name="Home"
                        options={{
                            title: "Vender",
                            headerStyle: {
                                elevation: 0,
                            },
                            headerTitleStyle: {
                                fontWeight: "bold",
                            },
                            headerRight: () => <HeaderButtons />,
                        }}
                    />
                    <Stack.Screen
                        component={Control}
                        name="Control"
                        options={{
                            title: "Comanda",
                        }}
                    />
                    <Stack.Screen
                        component={Checkout}
                        name="Checkout"
                        options={{
                            title: "Revisar pedido",
                        }}
                    />
                </Stack.Navigator>
                )
            }

        </>
    );
}

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('screen').width,
        height: Dimensions.get('screen').height,
        justifyContent: "center",
        backgroundColor: "#FFF",
        position: 'absolute',
        left: 0,
        top: 0,
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    }
});