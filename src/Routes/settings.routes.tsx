import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { useSelector } from "react-redux";

import Settings from "../Screens/Settings";

const Stack = createStackNavigator()

export default function SettingsRouter() {

    const loading = useSelector((state: any) => state.loading)

    return (
        <Stack.Navigator screenOptions={{ headerShown: true }}>
            <Stack.Screen name="Settings" component={Settings} options={
                {
                    title: 'Configurações'
                }
            } />
        </Stack.Navigator>

    );
}