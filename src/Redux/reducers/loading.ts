const INITIAL_STATE = false

//@ts-ignore
const Loading = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case "SHOW_LOADING":
            return action.loading
        default:
            return state;
    }
};

export default Loading;
