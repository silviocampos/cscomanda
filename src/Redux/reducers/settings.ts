import { ISettings } from "../../Interfaces";

const INITIAL_STATE: ISettings = {
    terminal: 0,
    baseURL: '192.168.0.1',
    port: 3000
}

//@ts-ignore
const Settings = (state = INITIAL_STATE, action) => {
    
    switch (action.type) {

        case 'SET_APP':

            return { ...state, ...action.data } as ISettings

        default:

            return state as ISettings;
    }
};

export default Settings;
