import { IMenu } from "../../Interfaces";

//@ts-ignore
const Menu = (state: IMenu = { items: [], titles: [] }, action) => {
    switch (action.type) {
        case "UPDATE_MENU":
            return { state, items: action.menu };

        case 'UPDATE_TITLE_MENU':

            return { ...state, titles: action.titles };
        // return [...state.filter(el => el.uuid != action.item.uuid)]

        case "GET_FILTERED":
            return [...state.items.filter(el => el.id_category != action.category)]

        default:
            return state;
    }
};

export default Menu;
