interface IAction {
    type: string;
    online: boolean;
}

const StatusAPI = (state = false, action: IAction) => {
    
    switch (action.type) {

        case 'SET_STATUS_API':
            
            return action.online

        default:
            return state;
    }
};

export default StatusAPI;
