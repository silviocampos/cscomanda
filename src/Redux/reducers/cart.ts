import { IProduct } from "../../Interfaces";

//@ts-ignore
const Cart = (state: IProduct[] = [], action) => {
  switch (action.type) {

    case "ADD_ITEM":

      return [...state, { ...action.item }];

    case 'REMOVE_ITEM':

      return [...state.filter(el => el.uuid != action.item.uuid)]

    case "UPDATE_ITEM":

      const itemIndexToUpdate = state.findIndex((el: IProduct) => (el.uuid === action.item.uuid))
      const newState: IProduct[] = [...state]
      newState[itemIndexToUpdate] = action.item
      
      return [...newState];

    case "CLEAR_CART":

      // console.log('acao', action)
      if (action && action.alert) alert('Pedido enviado com sucesso');
      return [];

    default:
      return state;
  }
};

export default Cart;
