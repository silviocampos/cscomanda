import { combineReducers } from "redux";

import settings from './settings'
import auth from "./auth";
import cart from "./cart";
import control from "./control";
import items from "./items";
import loading from './loading';
import menu from './menu';
import online from './online';

export default combineReducers({
  auth,
  settings,
  cart,
  control,
  items,
  loading,
  menu, 
  online
});
