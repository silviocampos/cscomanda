interface Action {
  type: string;
  data: any
}

const Auth = (state = {}, action: Action) => {
  switch (action.type) {

    case 'SET_AUTH':

      return { ...state, ...action.data }

    case 'CLEAR_AUTH':
      return {}

    default:
      return state;
  }
};

export default Auth;
