import { IControl, IControlState } from '../../Interfaces';

const INITIAL_STATE: IControlState = {
  active: null,
  list: [],
};

//@ts-ignore
const Comanda = (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case "ADD_CONTROL":

      return { ...state, active: action.control }

    case "ADD_LIST_CONTROL":

      if (action.controls)

        return { ...state, list: action.controls }

      else

        return { ...state, list: [] }

    case "CLEAR_ACTIVE_CONTROL":
      console.log(
        state.active,        
        state.list
      )

      return {
        ...state,
        active: null,
        list: [...state?.list?.filter(({ nroComanda }) => nroComanda != state.active?.nroComanda),
        state.active]
      }

    case "CLEAR_CONTROL":

      return [];

    default:
      return state;
  }
};

export default Comanda;
