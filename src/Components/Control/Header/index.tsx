import React, { useState } from "react";

import { Ionicons } from "@expo/vector-icons";
import { useSelector, useDispatch } from "react-redux";
import { useNavigation } from "@react-navigation/native";
import { FiLogOut } from 'react-icons/fi'
import { RectButton } from 'react-native-gesture-handler'
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";

import Colors from "../../../Style/colors";

export default function LogoutHeaderButton() {
    const dispatch = useDispatch()
    const navigation = useNavigation();

    const control = useSelector((state: any) => state.control)

    async function handlerLogout() {
        const dispatchs = ['CLEAR_AUTH', 'CLEAR_CART', 'CLEAR_CONTROL']
        await Promise.all(dispatchs.map(type => Promise.resolve(dispatch({ type }))))
        navigation.navigate("Login")
    }

    async function handlerSettings() {
        navigation.navigate("Settings")
    }

    return (
        <View style={styles.container}>
            <RectButton style={[styles.button, { marginEnd: 16 }]} onPress={handlerSettings}>
                <Text style={styles.labelButton}></Text>
                {/* Config */}
                <Ionicons name="md-settings" style={styles.icon} />
            </RectButton>
            <RectButton style={styles.button} onPress={handlerLogout}>
                <Text style={styles.labelButton}>Sair</Text>
                <Ionicons name="md-exit" style={styles.icon} />
            </RectButton>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 16,
        flexDirection: "row"
    },
    button: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",

    },
    icon: {
        paddingLeft: 8,
        fontSize: 26,
        color: Colors.Primary,
    },
    labelButton: {
        fontSize: 14,
        fontWeight: "bold",
    },
});
