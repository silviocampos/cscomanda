import React, { useState } from "react";

import { Ionicons } from "@expo/vector-icons";
import { useSelector } from "react-redux";
import { useNavigation } from "@react-navigation/native";

import { View, Text, TouchableOpacity, StyleSheet } from "react-native";

import Colors from "../../../Style/colors";

export default function HeaderButtons() {

  const navigation = useNavigation();

  const control = useSelector((state: any) => state.control)

  function handlerControl() {
    navigation.navigate("Control");
  }

  function handlerControlDetail() {
    navigation.navigate("ControlDetail", { control: control.active });
  }

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.button}
        onPress={handlerControl}
        onLongPress={handlerControlDetail}
      >

        {control?.active?.nroComanda &&
          <Text style={styles.labelButton}>COMANDA {control?.active?.nroComanda}</Text>
        }

        {!control?.active?.nroComanda &&
          <Text style={styles.labelButton}>SELECIONAR COMANDA</Text>
        }

        <Ionicons name="md-list-box" style={styles.icon} />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
  },
  button: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  icon: {
    paddingLeft: 8,
    fontSize: 26,
    color: Colors.Primary,
  },
  labelButton: {
    fontSize: 14,
    fontWeight: "bold",
  },
});
