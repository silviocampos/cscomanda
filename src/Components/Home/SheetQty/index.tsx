import React, { useRef, useEffect, ChangeEventHandler, useState } from "react";

import {
  Animated,
  View,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
} from "react-native";

import { RectButton } from 'react-native-gesture-handler'

import { Ionicons } from "@expo/vector-icons";
import Colors from "../../../Style/colors";
import { useSelector, useDispatch } from "react-redux";
import { IProduct } from "../../../Interfaces";
import Dialog from "../../../Util/Dialog";

interface Props {
  item: any;
  setItem: (product: IProduct) => void;
  change?: boolean
}

const SheetQt: React.FC<Props> = ({ item, setItem, change }) => {
  const dispatch = useDispatch();

  const heightAnim = useRef(new Animated.Value(0)).current;
  const opacityAnim = useRef(new Animated.Value(0)).current;

  const AnimatedTiming = (animated: any, toValue: any, callback?: any) =>
    Animated.timing(animated, { toValue, duration: 300 }).start(callback);

  const open = () => {
    AnimatedTiming(heightAnim, 300);
    AnimatedTiming(opacityAnim, 1);
  };

  const close = () => {
    AnimatedTiming(heightAnim, 0, () => setItem({} as IProduct));
    AnimatedTiming(opacityAnim, 0);
  };

  function handlerAddQtyItem() {
    setItem({ ...item, qty: item.qty + 1 });
  }

  function handlerRemoveQtyItem() {
    if (item.qty === 0) return;
    if (!change && (item.qty === 1)) return;

    setItem({ ...item, qty: item.qty - 1 });
  }

  function addItemCart(item: IProduct) {
    const total = item.qty * item.price;
    item.total = total
    dispatch({ type: "ADD_ITEM", item });
    close();
  }

  function updateItemCart(item: IProduct) {
    const method = (item.qty === 0) ? 'REMOVE_ITEM' : 'UPDATE_ITEM';

    const doUpdate = () => {
      const total = item.qty * item.price;
      item.total = total
      dispatch({ type: method, item });
      close();
    }

    if (method === 'REMOVE_ITEM') {
      Dialog.showAlert({
        message: `Deseja remover esse item [${item.description}]?`,
        isQuestion: true,
        actionYes: doUpdate
      })
    } else doUpdate()
  }

  function handlerModifyCart(item: IProduct) {
    if (change)
      return updateItemCart(item)

    return addItemCart(item)
  }

  useEffect(() => open(), []);
  return (
    <Animated.View
      style={[
        styles.container,
        {
          height: heightAnim, // Bind opacity to animated value
          opacity: opacityAnim,
        },
      ]}
    >
      <Text style={styles.itemTitle}>{item.description}</Text>
      <View style={styles.containerButtons}>
        <RectButton onPress={handlerRemoveQtyItem}>
          <Ionicons size={36} name="md-remove-circle" color="red" />
        </RectButton>
        <Text style={styles.labelQty}>{item.qty}</Text>

        <RectButton onPress={handlerAddQtyItem}>
          <Ionicons size={36} name="md-add-circle" color="green" />
        </RectButton>
      </View>
      <View style={styles.containerInputObservation}>
        <TextInput
          style={styles.inputObservation}
          placeholder="Observações..."
          value={item.note}
          onChangeText={(text) => setItem({ ...item, note: text })}></TextInput>
      </View>
      <View style={styles.containerButtonsBottom}>
        <RectButton
          style={[styles.buttonBottom, styles.buttonBottomCancel]}
          onPress={close}
        >
          <Text style={styles.buttonLabel}>Cancelar</Text>
        </RectButton>
        <RectButton
          style={[styles.buttonBottom, styles.buttonBottomAdd]}
          onPress={() => handlerModifyCart(item)}
        >
          <Text style={styles.buttonLabel}>{change ? 'Confirmar' : 'Adicionar'}</Text>
        </RectButton>
      </View>
    </Animated.View>
  );
}

const styles = StyleSheet.create({
  container: {
    borderTopColor: "#EEE",
    borderTopWidth: 1,
    padding: 16,
  },
  itemTitle: {
    fontSize: 22,
    textAlign: "center",
    padding: 16,
    marginBottom: 16,
  },
  containerButtons: {
    width: "50%",
    flex: 1,
    marginLeft: "auto",
    marginRight: "auto",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    alignContent: "center",
  },
  containerButtonsBottom: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end",
  },
  containerInputObservation: {
    padding: 16,
    marginBottom: 32
  },
  buttonBottom: {
    padding: 16,
    marginVertical: 16,
    width: "40%",
    borderRadius: 4,
  },
  buttonBottomAdd: {
    backgroundColor: Colors.Success,
  },
  buttonBottomCancel: {
    backgroundColor: Colors.Danger,
  },
  buttonLabel: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 16,
    color: "#FFF",
  },
  labelQty: {
    fontSize: 36,
    textAlign: "center",
  },
  inputSearch: {
    backgroundColor: "#f5f5f5",
    padding: 16,
    fontSize: 18,
    textAlign: "center",
  },
  inputObservation: {
    height: 50,
    padding: 16,
    backgroundColor: "#FFF",
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#EEE'
  },
});



export default SheetQt;